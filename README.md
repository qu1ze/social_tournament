# Social Tournament on Go

Specific tournament application which provides isolated transaction locks using `SELECT ... FOR UPDATE` statements.
Its built using gin-gonic for routing, gorm as ORM and viper for configuration stuff.

### Getting started

1. Clone this repository
2. `docker-compose build`
3. Start database service first `docker-compose up -d tournament_db` (to be available before app starts)
4. `docker-compose up -d tournament_app`
